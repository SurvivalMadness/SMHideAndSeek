package me.xxmatthdxx.hideandseak.listeners;

import me.xxmatthdxx.hideandseak.game.GameManager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

/**
 * Created by Matthew on 2015-06-06.
 */
public class PlayerMove implements Listener {

    @EventHandler
    public void onMove(PlayerMoveEvent e){
        if(!GameManager.getInstance().isSeekerCanMove()){
            if(GameManager.getInstance().getSeekers().contains(e.getPlayer().getUniqueId())){
                e.setTo(e.getFrom());
            }
            else {
                return;
            }
        }
    }
}
