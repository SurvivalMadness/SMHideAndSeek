package me.xxmatthdxx.hideandseak.listeners;

import me.xxmatthdxx.hideandseak.game.GameManager;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

/**
 * Created by Matthew on 2015-06-06.
 */
public class DamageEvent implements Listener {

    private GameManager game = GameManager.getInstance();

    @EventHandler(priority = EventPriority.HIGH)
    public void onDamage(EntityDamageByEntityEvent e) {
        if (e.getDamager() instanceof Player && e.getEntity() instanceof Player) {

            Player tagger = (Player) e.getDamager();
            Player dead = (Player) e.getEntity();

            if (game.getHiders().contains(dead.getUniqueId()) && game.getHiders().contains(tagger.getUniqueId()) || game.getSeekers().contains(dead.getUniqueId()) && game.getSeekers().contains(tagger.getUniqueId())) {
                e.setCancelled(true);
            } else if (game.getHiders().contains(dead.getUniqueId()) && game.getSeekers().contains(tagger.getUniqueId())) {
                //Tagged/Found
                game.addSeeker(dead, tagger);
            }
        }
        return;
    }
}
