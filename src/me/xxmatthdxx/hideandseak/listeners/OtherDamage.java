package me.xxmatthdxx.hideandseak.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;

/**
 * Created by Matthew on 2015-06-06.
 */
public class OtherDamage implements Listener {

    @EventHandler
    public void onDamage(EntityDamageEvent e){
        if(!(e.getCause() == EntityDamageEvent.DamageCause.ENTITY_ATTACK)){
            e.setCancelled(true);
        }
    }
}
