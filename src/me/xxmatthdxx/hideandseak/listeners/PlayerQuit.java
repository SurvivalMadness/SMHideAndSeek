package me.xxmatthdxx.hideandseak.listeners;

import main.java.RedisManager;
import me.xxmatthdxx.hideandseak.game.GameManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

/**
 * Created by Matthew on 2015-06-06.
 */
public class PlayerQuit implements Listener {

    @EventHandler
    public void onQuit(PlayerQuitEvent e){
        RedisManager.getInstance().setPlayers(Bukkit.getServerName());

        Player pl = e.getPlayer();

        if(Bukkit.getOnlinePlayers().size() < 2){
            GameManager.getInstance().stop(false);
            Bukkit.broadcastMessage(ChatColor.GOLD + "Not enough players to continue the game!");
            return;
        }

        if(GameManager.getInstance().getHiders().contains(pl.getUniqueId())){
            if((GameManager.getInstance().getHiders().size() - 1) < 1 && GameManager.getInstance().getSeekers().size() >=1){
                GameManager.getInstance().stop(true);
                GameManager.getInstance().getHiders().remove(pl.getUniqueId());
                return;
            }
        }
        else if(GameManager.getInstance().getSeekers().contains(pl.getUniqueId())){
            if((GameManager.getInstance().getSeekers().size() -1) < 1 && GameManager.getInstance().getHiders().size() >= 2){
                GameManager.getInstance().chooseSeeker();
                GameManager.getInstance().getSeekers().remove(pl.getUniqueId());
                return;
            }
        }
    }
}
