package me.xxmatthdxx.hideandseak.listeners;

import me.xxmatthdxx.hideandseak.utils.ScoreboardUtils;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

/**
 * Created by Matthew on 2015-06-06.
 */
public class PlayerJoin implements Listener {

    @EventHandler
    public void onJoin(PlayerJoinEvent e){
        ScoreboardUtils.createLobbyScoreboard(e.getPlayer());
        e.getPlayer().getInventory().clear();
    }
}
