package me.xxmatthdxx.hideandseak.game;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import main.java.RedisManager;
import main.java.SMCore;
import main.java.game.GameState;
import main.java.utils.Title;
import me.xxmatthdxx.hideandseak.Main;
import org.bukkit.*;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.*;

/**
 * Created by Matthew on 2015-06-06.
 */
public class GameManager {

    private Main plugin = Main.getPlugin();

    private static GameManager instance = new GameManager();

    public static GameManager getInstance() {
        return instance;
    }

    private Arena currentArena;

    private boolean seekerCanMove;

    private List<UUID> seekers = new ArrayList<>();
    private List<UUID> hiders = new ArrayList<>();
    private List<Arena> arenas = new ArrayList<>();

    public List<Arena> getArenas() {
        return arenas;
    }

    public boolean isSeekerCanMove() {
        return seekerCanMove;
    }

    public void start() {
        Main.getPlugin().setState(GameState.INGAME);
        Main.getPlugin().setTicks(60 * 8);

        chooseSeeker();

        for (Player pl : Bukkit.getOnlinePlayers()) {
            pl.teleport(currentArena.getSpawn());
            pl.playSound(pl.getLocation(), Sound.LEVEL_UP, 1, 1);
            pl.setHealth(pl.getMaxHealth());
            pl.setFlying(false);
            pl.setAllowFlight(false);
            pl.setGameMode(GameMode.SURVIVAL);
            pl.getInventory().setHeldItemSlot(0);
            pl.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, 20*8 * 60, 1));
            if(seekers.contains(pl.getUniqueId())){
                continue;
            }
            else {
                hiders.add(pl.getUniqueId());
            }
        }
        setupPlayers();
        RedisManager.getInstance().setState(Bukkit.getServerName(), Main.getPlugin().getState());
        RedisManager.getInstance().setMap(Bukkit.getServerName(), getCurrentArena().getName());
    }

    public void setupPlayers() {

        ItemStack chest = new ItemStack(Material.LEATHER_CHESTPLATE, 1);
        LeatherArmorMeta chestMeta = (LeatherArmorMeta) chest.getItemMeta();
        chestMeta.setColor(Color.WHITE);
        chest.setItemMeta(chestMeta);

        ItemStack seekerChest = new ItemStack(Material.LEATHER_CHESTPLATE, 1);
        LeatherArmorMeta seekerMeta = (LeatherArmorMeta) seekerChest.getItemMeta();
        seekerMeta.setColor(Color.RED);
        seekerChest.setItemMeta(seekerMeta);

        for (Player pl : Bukkit.getOnlinePlayers()) {
            if (seekers.contains(pl.getUniqueId())) {
                pl.getInventory().setChestplate(seekerChest);
            } else {
                pl.getInventory().setChestplate(chest);
            }
        }
    }

    public Arena getArena(String name){
        for(Arena a : arenas){
            if(a.getName().equalsIgnoreCase(name)){
                return a;
            }
        }
        return null;
    }

    public void chooseSeeker() {
        Random ran = new Random();
        int ranInt = ran.nextInt(Bukkit.getOnlinePlayers().size());

        Player pl = (Player) Bukkit.getOnlinePlayers().toArray()[ranInt];
        seekers.add(pl.getUniqueId());
        Bukkit.broadcastMessage(ChatColor.RED + ChatColor.BOLD.toString() + pl.getName() + " is the seeker! You have 20 seconds to hide");
        pl.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 20 * 15, 9));
        seekerCanMove = false;
        new BukkitRunnable() {
            public void run() {
                seekerCanMove = true;
            }
        }.runTaskLater(Main.getPlugin(), 20 * 15);
    }

    public String getMapChosen() {
        int current = 0;
        Arena toChose = arenas.get(0);
        for (Arena a : arenas) {
            if (a.getVotes() > current) {
                current = a.getVotes();
                toChose = a;
            }
        }
        currentArena = toChose;
        return toChose.getName();
    }

    public Arena getCurrentArena() {
        return currentArena;
    }

    public List<UUID> getSeekers() {
        return seekers;
    }

    public List<UUID> getHiders() {
        return hiders;
    }

    public void stop(boolean winner) {
        hiders.clear();
        seekers.clear();
        if (winner) {
            if (hiders.size() <= 0 && seekers.size() > 0) {
                //Seekers win
                for (final Player pl : Bukkit.getOnlinePlayers()) {
                    Title.sendTitle(pl, ChatColor.GOLD + "Seekers Win!", "You will be teleported soon", 20, 20, 20);
                }

                new BukkitRunnable() {
                    public void run() {
                        for (Player pl : Bukkit.getOnlinePlayers()) {
                            sendToHub(pl);
                        }
                    }
                }.runTaskLater(Main.getPlugin(), 20 * 5);
            }
            else if(hiders.size() > 0){
                //Hiders win
                for (final Player pl : Bukkit.getOnlinePlayers()) {
                    Title.sendTitle(pl, ChatColor.GOLD + "Hiders Win!", "You will be teleported soon", 20, 20, 20);
                }

                new BukkitRunnable() {
                    public void run() {
                        for (Player pl : Bukkit.getOnlinePlayers()) {
                            sendToHub(pl);
                        }
                    }
                }.runTaskLater(Main.getPlugin(), 20 * 5);
            }
        }
        plugin.setTicks(45);
        plugin.setState(GameState.JOIN);
        for(Arena a : arenas){
            a.setVotes(0);
        }
        RedisManager.getInstance().setState(Bukkit.getServerName(), Main.getPlugin().getState());
        RedisManager.getInstance().setMap(Bukkit.getServerName(), "Voting");
    }

    public void sendToHub(Player pl) {
        String hub = "hub";
        ByteArrayDataOutput out = ByteStreams.newDataOutput();
        out.writeUTF("Connect");
        out.writeUTF(hub);
        pl.sendPluginMessage(SMCore.getPlugin(), "BungeeCord", out.toByteArray());
    }

    public void addSeeker(Player pl, Player tagger){
        Bukkit.broadcastMessage(ChatColor.GRAY + ChatColor.BOLD.toString() + pl.getName() + ChatColor.GOLD + ChatColor.BOLD.toString() + " has been found by " + ChatColor.GRAY + ChatColor.BOLD.toString() + tagger.getName());
        if(hiders.size() - 1 <= 0){
            hiders.remove(pl.getUniqueId());
            stop(true);
            //Stop le game
        }
        else {
            hiders.remove(pl.getUniqueId());
            seekers.add(pl.getUniqueId());
            Title.sendTitle(pl, ChatColor.DARK_AQUA + "You were found!", ChatColor.GOLD + "You are now a seeker!", 20,20,20);

            ItemStack seekerChest = new ItemStack(Material.LEATHER_CHESTPLATE, 1);
            LeatherArmorMeta seekerMeta = (LeatherArmorMeta) seekerChest.getItemMeta();
            seekerMeta.setColor(Color.RED);
            seekerChest.setItemMeta(seekerMeta);
            pl.getInventory().setChestplate(seekerChest);
        }
    }

    public void setup(){
        if (plugin.getConfig().getConfigurationSection("arenas") == null) {
            plugin.getConfig().createSection("arenas");
            return;
        }

        for (String s : plugin.getConfig().getConfigurationSection("arenas").getKeys(false)) {
            String[] split = plugin.getConfig().getString("arenas." + s + ".spawn").split(",");
            String[] p1Split = plugin.getConfig().getString("arenas." + s + ".p1").split(",");
            String[] p2Split = plugin.getConfig().getString("arenas." + s + ".p2").split(",");

            Location spawn = new Location(Bukkit.getWorld(split[3]), Double.valueOf(split[0]), Double.valueOf(split[1]), Double.valueOf(split[2]));
            Location p1 = new Location(Bukkit.getWorld(p1Split[3]), Double.valueOf(p1Split[0]), Double.valueOf(p1Split[1]), Double.valueOf(p1Split[2]));
            Location p2 = new Location(Bukkit.getWorld(p2Split[3]), Double.valueOf(p2Split[0]), Double.valueOf(p2Split[1]), Double.valueOf(p2Split[2]));

            Arena a = new Arena(s, spawn, p1, p2);
            arenas.add(a);
            a.setSpawn(spawn);
            a.setP1(p1);
            a.setP2(p2);
            System.out.println("Loaded arena " + a.getName());
        }
    }

    public HashMap<UUID, Arena> getPlayerVotes() {
        return playerVotes;
    }

    private HashMap<UUID, Arena> playerVotes = new HashMap<>();
}
