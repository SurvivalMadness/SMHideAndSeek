package me.xxmatthdxx.hideandseak.game;

import main.java.game.GameState;
import me.xxmatthdxx.hideandseak.Main;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.scheduler.BukkitRunnable;

/**
 * Created by Matthew on 2015-06-06.
 */
public class GameTimer extends BukkitRunnable {

    private Main plugin = Main.getPlugin();
    private GameManager game = GameManager.getInstance();

    public final int VOTING_TIME = 45;

    private static GameTimer instance = new GameTimer();

    public static GameTimer getInstance(){
        return instance;
    }

    @Override
    public void run() {
        if(plugin.getTicks() > 0){
            plugin.setTicks(plugin.getTicks() - 1);
        }

        if(plugin.getState() == GameState.JOIN){
            if(plugin.getTicks() % 15 == 0 && plugin.getTicks() != 0){
                Bukkit.broadcastMessage(ChatColor.GOLD + "Vote ending in " + ChatColor.GRAY + plugin.getTicks());
                return;
            }
            if(plugin.getTicks() < 5 && plugin.getTicks() != 0){
                Bukkit.broadcastMessage(ChatColor.GOLD + "Vote ending in " + ChatColor.GRAY + plugin.getTicks());
                return;
            }
            else if(plugin.getTicks() == 0){
                if(Bukkit.getOnlinePlayers().size() < 2){
                    Bukkit.broadcastMessage(ChatColor.GOLD + "Not enough players to start a game!");
                    plugin.setTicks(VOTING_TIME);
                    return;
                }
                game.getMapChosen();
                Bukkit.broadcastMessage(game.getCurrentArena().getName() + " has been chosen with " + game.getCurrentArena().getVotes() + " vote(s)");
                game.start();
            }
        }
        else if(plugin.getState() == GameState.INGAME){
            if(plugin.getTicks() % 30 == 0 && plugin.getTicks() != 0){
                Bukkit.broadcastMessage(ChatColor.GOLD + "Game ending in " + ChatColor.GRAY + plugin.getTicks() + " seconds");
                return;
            }
            if(plugin.getTicks() < 5 && plugin.getTicks() != 0){
                Bukkit.broadcastMessage(ChatColor.GOLD + "Game ending in " + ChatColor.GRAY + plugin.getTicks() + " second");
                return;
            }
            else if(plugin.getTicks() == 0){
                game.stop(true);
            }
        }
    }
}
