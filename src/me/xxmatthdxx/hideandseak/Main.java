package me.xxmatthdxx.hideandseak;

import main.java.game.GameState;
import me.xxmatthdxx.hideandseak.cmds.CommandManager;
import me.xxmatthdxx.hideandseak.game.GameManager;
import me.xxmatthdxx.hideandseak.game.GameTimer;
import me.xxmatthdxx.hideandseak.listeners.*;
import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Created by Matthew on 2015-06-06.
 */
public class Main extends JavaPlugin {

    private static Main plugin;

    private int ticks;
    private GameState state;

    public void onEnable() {
        plugin = this;
        PluginManager pm = Bukkit.getPluginManager();
        CommandManager cm = new CommandManager();

        if (getDataFolder().exists()) {
            saveConfig();
        } else {
            getConfig().options().copyDefaults(true);
            saveConfig();
        }

        GameManager.getInstance().setup();

        getCommand("game").setExecutor(cm);
        getCommand("vote").setExecutor(cm);

        pm.registerEvents(new DamageEvent(), this);
        pm.registerEvents(new FoodLevelChange(), this);
        pm.registerEvents(new OtherDamage(), this);
        pm.registerEvents(new PlayerJoin(), this);
        pm.registerEvents(new PlayerMove(), this);
        ticks = 50;
        state = GameState.JOIN;
        GameTimer.getInstance().runTaskTimer(this, 0, 20);
    }

    public void onDisable() {
        plugin = null;
    }

    public int getTicks() {
        return ticks;
    }

    public void setTicks(int ticks) {
        this.ticks = ticks;
    }

    public GameState getState() {
        return state;
    }

    public void setState(GameState state) {
        this.state = state;
    }

    public static Main getPlugin() {
        return plugin;
    }
}
