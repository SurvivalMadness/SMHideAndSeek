package me.xxmatthdxx.hideandseak.utils;

import me.xxmatthdxx.hideandseak.game.Arena;
import me.xxmatthdxx.hideandseak.game.GameManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Scoreboard;

/**
 * Created by Matthew on 2015-06-06.
 */
public class ScoreboardUtils {
    private static Scoreboard board;
    private static Objective objective;
    private static String name;

    public static void createLobbyScoreboard(Player player){
        board = Bukkit.getScoreboardManager().getNewScoreboard();
        name = ChatColor.RED + "Vote for map";
        objective = board.registerNewObjective("test", "dummy");
        objective.setDisplaySlot(DisplaySlot.SIDEBAR);
        objective.setDisplayName(name);
        for(Arena a : GameManager.getInstance().getArenas()){
            Score score = objective.getScore(a.getName());
            score.setScore(a.getVotes());
        }
        player.setScoreboard(board);
    }
}
