package me.xxmatthdxx.hideandseak.cmds;

import com.sk89q.worldedit.bukkit.WorldEditPlugin;
import com.sk89q.worldedit.bukkit.selections.Selection;
import main.java.game.GameState;
import me.xxmatthdxx.hideandseak.Main;
import me.xxmatthdxx.hideandseak.game.Arena;
import me.xxmatthdxx.hideandseak.game.GameManager;
import me.xxmatthdxx.hideandseak.utils.ScoreboardUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created by Matthew on 2015-06-06.
 */
public class CommandManager implements CommandExecutor {

    private Main plugin = Main.getPlugin();

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String s, String[] args) {
        if (cmd.getName().equalsIgnoreCase("game")) {
            if (!(sender instanceof Player)) {
                sender.sendMessage(ChatColor.RED + "Player only command!");
            } else {
                Player pl = (Player) sender;
                if (!pl.hasPermission("pi.game")) {
                    pl.sendMessage(ChatColor.RED + "Unknown Command!");
                    return true;
                }

                if (args.length == 0) {
                    pl.sendMessage(ChatColor.RED + "/game start");
                    pl.sendMessage(ChatColor.RED + "/game stop");
                    return true;
                } else if (args.length == 1) {
                    if (args[0].equalsIgnoreCase("start")) {
                        if (Bukkit.getOnlinePlayers().size() <= 1) {
                            pl.sendMessage(ChatColor.RED + "Not enough players!");
                            return true;
                        }
                        GameManager.getInstance().getMapChosen();
                        GameManager.getInstance().start();
                    } else if (args[0].equalsIgnoreCase("stop")) {
                        GameManager.getInstance().stop(false);
                    }
                } else if (args.length == 2) {
                    if (args[0].equalsIgnoreCase("create")) {
                        String name = args[1];

                        if (GameManager.getInstance().getArena(name) != null) {
                            pl.sendMessage(ChatColor.RED + "Arena already exists!");
                            return true;
                        }

                        Location loc = pl.getLocation();

                        Selection sel = getWorldEdit().getSelection(pl);

                        if (sel == null) {
                            pl.sendMessage(ChatColor.RED + "Arena needs a selection with world edit!");
                            return true;
                        }

                        Location p1 = sel.getMinimumPoint();
                        Location p2 = sel.getMaximumPoint();

                        plugin.getConfig().set("arenas." + name + ".spawn", loc.getX() + "," + loc.getY() + "," + loc.getZ() + "," + loc.getWorld().getName());
                        plugin.saveConfig();

                        plugin.getConfig().set("arenas." + name + ".p1", p1.getX() + "," + p1.getY() + "," + p1.getZ() + "," + p1.getWorld().getName());
                        plugin.saveConfig();

                        plugin.getConfig().set("arenas." + name + ".p2", p2.getX() + "," + p2.getY() + "," + p2.getZ() + "," + p2.getWorld().getName());
                        plugin.saveConfig();
                        plugin.reloadConfig();
                        Arena a = new Arena(name, loc, p1, p2);
                        GameManager.getInstance().getArenas().add(a);
                        pl.sendMessage(ChatColor.GREEN + "Created arena: " + a.getName());
                    } else if (args[0].equalsIgnoreCase("delete")) {
                        String name = args[1];

                        if (GameManager.getInstance().getArena(name) == null) {
                            pl.sendMessage(ChatColor.RED + "No arena in memory!");
                            return true;
                        } else {

                            if (GameManager.getInstance().getCurrentArena() == GameManager.getInstance().getArena(name)) {
                                pl.sendMessage(ChatColor.RED + "Game is in progress, please wait!");
                                return true;
                            }

                            plugin.getConfig().set("arenas." + name, null);
                            plugin.saveConfig();
                            plugin.reloadConfig();
                            GameManager.getInstance().getArenas().remove(GameManager.getInstance().getArena(name));
                            pl.sendMessage(ChatColor.RED + "Destroyed arena!");
                            return true;
                        }
                    }
                }
            }
        } else if (cmd.getName().equalsIgnoreCase("vote")) {
            if (!(sender instanceof Player)) {
                return true;
            }
            Player pl = (Player) sender;

            if (plugin.getState() != GameState.JOIN) {
                pl.sendMessage(ChatColor.RED + "You cannot vote at this time!");
                return true;
            }

            if (args.length == 0) {
                for (Arena a : GameManager.getInstance().getArenas()) {
                    pl.sendMessage(a.getName());
                }
            } else if (args.length == 1) {
                String name = args[0];

                if (GameManager.getInstance().getArena(name) == null) {
                    pl.sendMessage(ChatColor.RED + "No arena with that name!");
                    return true;
                } else {
                    Arena a = GameManager.getInstance().getArena(name);
                    if (GameManager.getInstance().getPlayerVotes().containsKey(pl.getUniqueId())) {
                        Arena toRemove = GameManager.getInstance().getPlayerVotes().get(pl.getUniqueId());
                        toRemove.removeVote();
                        a.addVote();
                        GameManager.getInstance().getPlayerVotes().remove(pl.getUniqueId());
                        GameManager.getInstance().getPlayerVotes().put(pl.getUniqueId(), a);
                        pl.sendMessage(ChatColor.RED + "Changed your vote to: " + a.getName());

                    }
                    GameManager.getInstance().getPlayerVotes().put(pl.getUniqueId(), a);
                    a.addVote();
                    pl.sendMessage(ChatColor.RED + "Added your vote to: " + a.getName());
                }

                for (Player p : Bukkit.getOnlinePlayers()) {
                    ScoreboardUtils.createLobbyScoreboard(p);
                }
            }
        }
        return false;
    }

    public WorldEditPlugin getWorldEdit() {
        return (WorldEditPlugin) Bukkit.getPluginManager().getPlugin("WorldEdit");
    }
}
